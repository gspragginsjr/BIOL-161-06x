// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "babel-polyfill";
import Vue from 'vue';
import svg4everybody from 'svg4everybody';
import objectFitImages from 'object-fit-images';
import velocity from 'velocity-animate';
import velocityUI from 'velocity-animate/velocity.ui';
import {TweenMax, Power2, TimelineLite} from "gsap";
import DrawSVGPlugin from "./DrawSVGPlugin.js";
import es6Promise from 'es6-promise';

import GlobalHeader from './GlobalHeader';
import GlobalFooter from './GlobalFooter';
import GlobalSidebar from './GlobalSidebar';
import GlobalHero from './GlobalHero';
import GlobalSubhero from './GlobalSubhero';
import GlobalVideo from './GlobalVideo';
import GlobalCarousel from './GlobalCarousel';
import GlobalFadeCarousel from './GlobalFadeCarousel';
import GlobalFilter from './GlobalFilter';
import GlobalIntroContent from './GlobalIntroContent';
import GlobalIntroContentBlock from './GlobalIntroContentBlock';
import GlobalCircleImageContent from './GlobalCircleImageContent';
import GlobalLargeImageContent from './GlobalLargeImageContent';
import GlobalContentBlock from './GlobalContentBlock';
import GlobalFeatured from './GlobalFeatured';
import GlobalFactsDivider from './GlobalFactsDivider';
import GlobalFullBleedSection from './GlobalFullBleedSection';
import GlobalButtonDivider from './GlobalButtonDivider';
import GlobalQuote from './GlobalQuote';
import GlobalBoxList from './GlobalBoxList';
import GlobalEventList from './GlobalEventList';
import GlobalPostIndex from './GlobalPostIndex';
import GlobalPageEnder from './GlobalPageEnder';
import GlobalHighlights from './GlobalHighlights';
import GuideBlock from './GuideBlock';
import GuideColors from './GuideColors';
import GuideTypography from './GuideTypography';
import GuideButtons from './GuideButtons';
import GuideTables from './GuideTables';
import ModuleButtonScaffolding from './ModuleButtonScaffolding';
import HeroHome from './HeroHome';
import HeroVideo from './HeroVideo';
import HeroColumn from './HeroColumn';
import HomeWidget from './HomeWidget';
import HomeAbout from './HomeAbout';
import HomeCarousel from './HomeCarousel';
import AboutCarousel from './AboutCarousel';
import AboutTest from './AboutTest';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
  	GlobalHeader,
  	GlobalFooter,
  	GlobalSidebar,
  	GlobalHero,
  	GlobalSubhero,
  	GlobalVideo,
  	GlobalCarousel,
    GlobalFadeCarousel,
  	GlobalFilter,
    GlobalIntroContent,
    GlobalIntroContentBlock,
    GlobalCircleImageContent,
    GlobalLargeImageContent,
    GlobalContentBlock,
    GlobalFeatured,
    GlobalFactsDivider,
    GlobalFullBleedSection,
    GlobalButtonDivider,
  	GlobalQuote,
  	GlobalBoxList,
  	GlobalEventList,
    GlobalPostIndex,
  	GlobalHighlights,
  	GlobalPageEnder,
  	GuideBlock,
  	GuideColors,
  	GuideTypography,
  	GuideButtons,
  	GuideTables,
  	ModuleButtonScaffolding,
    HeroVideo,
    HeroHome,
    HeroColumn,
    HomeWidget,
    HomeAbout,
    HomeCarousel,
    AboutCarousel,
    AboutTest
  },
});

svg4everybody();
objectFitImages();
es6Promise.polyfill();

var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var looseyScript = document.createElement('script');
looseyScript.appendChild(document.createTextNode('var videoList = []; var readyYoutubeVids = [];'));
firstScriptTag.parentNode.insertBefore(looseyScript, firstScriptTag);

window.onYouTubePlayerAPIReady = function() {

    window.addEventListener("load", function() {

	    for (var video in window.videoList) {
	    	window.readyYoutubeVids.push(new YT.Player(window.videoList[video]));
	    }
	});
}
