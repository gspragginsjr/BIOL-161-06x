var utils = require('./utils')
var webpack = require('webpack')
var config = require('../config')
var merge = require('webpack-merge')
var baseWebpackConfig = require('./webpack.base.conf')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
var StylelintPlugin = require('stylelint-webpack-plugin')

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
  baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name])
})

module.exports = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({ sourceMap: config.dev.cssSourceMap })
  },
  // cheap-module-eval-source-map is faster for development
  devtool: '#cheap-module-eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    // https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'components.html',
      template: 'components.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'major-details.html',
      template: 'major-details.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'admissions.html',
      template: 'admissions.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'undergraduate-admissions-process.html',
      template: 'undergraduate-admissions-process.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'first-time-admissions.html',
      template: 'first-time-admissions.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'visit-campus.html',
      template: 'visit-campus.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'financial-aid.html',
      template: 'financial-aid.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'news-events.html',
      template: 'news-events.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'majors-degrees-programs.html',
      template: 'majors-degrees-programs.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'undergrad-admissions.html',
      template: 'undergrad-admissions.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'student-life.html',
      template: 'student-life.html',
      inject: true
    }),
    new HtmlWebpackPlugin({
      filename: 'home.html',
      template: 'home.html',
      inject: true
    }),
    new StylelintPlugin({
    files: ['src/**/*.vue']
    }),
    new FriendlyErrorsPlugin()
  ]
})
